package com.qinhailin;

import com.jfinal.server.undertow.UndertowServer;
import com.qinhailin.common.config.MainConfig;

public class Bootstrap {
    /**
     *运行main方法启动项目
     */
    public static void main(String[] args) {
        UndertowServer.start(MainConfig.class);
    }

}
