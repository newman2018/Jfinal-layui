package com.qinhailin.common.base;

import com.jfinal.plugin.activerecord.dialect.OracleDialect;

public class DMDialect extends OracleDialect {
    @Override
    public boolean isOracle() {
        return false;
    }
    public boolean isDm(){
        return true;
    }
}
